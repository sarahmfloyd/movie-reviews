import { useState } from "react"
import { useNavigate} from 'react-router-dom'
import api from "../../api/axiosConfig";


function Login() {
      const [username, setUsername] = useState('');
      const [password, setPassword] = useState('');
      const navigate = useNavigate();

      const handleLogin = async (e) => {
        e.preventDefault();
        try {
            // Send the login credentials to the backend
            const response = await api.post("/api/v1/login", {
              username: username,
              password: password,
            });

            // If the login is successful, redirect to the home page
            if (response.data === 'Login successful!') {
                navigate('/');
            } else {
              // Handle login error here (e.g., show an error message)
              console.log('Login failed:', response.data);
            }
        } catch (error){
            console.log(error)
        }
    }
        // Redirect the user to the home page after successful login
      return (
        <div className="login-container">
          <form className="login-form" onSubmit={handleLogin}>
            <h2>Login</h2>
            <div className="form-group">
              <label htmlFor="username">Username:</label>
              <input
                type="text"
                id="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password:</label>
              <input
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <button type="submit" className="login-button">
              Login
            </button>
          </form>
        </div>
      );
    };
export default Login;
